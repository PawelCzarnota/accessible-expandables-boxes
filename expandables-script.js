var num_box;
var collapsed;
function resize(theid){
    if(collapsed[theid]){
        $("#img"+theid).css("transform","rotate(90deg)");
        $("#a"+theid).show(400);
        collapsed[theid] = false;
    }else{
        $("#img"+theid).css("transform","rotate(0deg)");
        $("#a"+theid).hide(400);
        collapsed[theid] = true;
    }
}
function expandAll(){
	$(".arrow-right").css("transform", "rotate(90deg)");
    $(".contentbox").show(400);
	for(var count = 0; count<collapsed.length; count++){
	    	collapsed[count] = false;
		}
}
function collapseAll(){
    $(".arrow-right").css("transform", "rotate(0deg)");
    $(".contentbox").hide(400);
	for(var count = 0; count<collapsed.length; count++){
	    collapsed[count] = true;
	    }
}
function initCollapseAll(){
    $(".arrow-right").css("transform", "rotate(0deg)");
    $(".contentbox").hide(0);
	for(var count = 0; count<collapsed.length; count++){
	    collapsed[count] = true;
	    }
}
function init(){
    num_box = $(".contentbox").length
    collapsed = new Array(num_box); //collapsed is true when the box is collapsed
    initCollapseAll();
}
$(document).ready(init);
